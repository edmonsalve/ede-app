import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'reducestr'})
export class ReduceStringPipe implements PipeTransform {
    transform (value: string, charCount: number, addPoints?: boolean) {
        if (!value) {
            return value;
        }
        let reducedString = value.substr(0, charCount);
        reducedString = reducedString.charAt(0).toUpperCase() + reducedString.toLowerCase().slice(1)
        if (addPoints) reducedString = reducedString + '...';

        return reducedString;
    }
}