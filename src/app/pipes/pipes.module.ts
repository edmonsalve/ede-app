import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ReduceStringPipe } from './reduce-string.pipe';

@NgModule({
  declarations: [
    ReduceStringPipe,
  ],
  exports: [ReduceStringPipe],
})
export class PipesModule {}