import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from "@ionic/storage";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { InAppBrowser } from '@ionic-native/in-app-browser'

import { MyApp } from './app.component';
import { UserSignupPage } from '../pages/user-signup/user-signup';
import { EventsProvider } from './services/events.service';
import { AuthService } from './services/auth.service';
import { TransactionsService } from './services/transactions.service';


@NgModule({
  declarations: [
    MyApp,
    UserSignupPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UserSignupPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    EventsProvider,
    AuthService,
    InAppBrowser,
    TransactionsService,
    BarcodeScanner
  ]
})
export class AppModule {}
