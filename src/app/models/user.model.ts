export default class User {
    public name: string;
    public lastname: string;
    public password: string;
    public signature: string;
    public age: string;
    public gender: string;
    public location: string;
    public email: string;
    public role: string;
    public status: number;
    public created_at: string;

    constructor() {
        this.name="";
        this.lastname="";
        this.location="";
        this.age="";
        this.gender="";
        this.email="";
        this.status = 0;
    }
}