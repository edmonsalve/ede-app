import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import User from '../models/user.model';

import { environment as ENV } from '../../../src/environments/environment';

@Injectable()
export class AuthService {

    private baseUrl: string = ENV.BaseUrl;
    private isLogged: boolean = false;

    constructor (
        private http: Http
    ) {
        console.log(this.baseUrl);
    }

    public login (email: string, password: string): Observable<object[]> {
        const endpoint = this.baseUrl + "/users/login";
        const user = { email, password, isMobile: true };
        return this.http.post(endpoint, user)
                .map(res => res.json())
                .catch((e) => {
                    return Observable.throw(e);
                });
    }

    public signUp (user: User) {
        const endpoint = this.baseUrl + '/users/signup';
        return this.http.post(endpoint, {...user, isMobile: true})
        .map(res => res.json())
        .catch((e) => {
            return Observable.throw(e);
        });
    }

    public recoveryPassword (userEmail: string) {
        const endpoint = this.baseUrl + '/users/recoveryPassword';
        return this.http.post(endpoint, { userEmail })
        .map(res => res.json())
        .catch((e) => {
            return Observable.throw(e);
        });
    }
}