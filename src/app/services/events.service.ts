import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { environment as ENV } from '../../../src/environments/environment';
import { Storage } from '@ionic/storage';

@Injectable()
export class EventsProvider {

    private baseUrl: string = ENV.BaseUrl;

    constructor (public http: Http, private storage: Storage) {
        
    }
    
    public getEvents (): Observable<object[]> {
        const endpoint = this.baseUrl + "/events/getAll";
        return this.http.get(endpoint)
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            });
    }

    public validateTicket(data, userToken): Observable<object> {
        const endpoint = this.baseUrl + "/events/validateTicket";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, data, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public subscribeToGuestlist(userName, userDoc, userCellphone, userEmail, eventId, userToken): Observable<any> {
        const endpoint = this.baseUrl + "/events/guestlist";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, { userName, eventId, userDoc, userCellphone, userEmail }, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            });
    }

    public verifyPendingTicket(userId, eventId, userToken): Observable<any> {
        const endpoint = this.baseUrl + "/events/verifyPendingTicket";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, { userId, eventId }, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            });
    }

    public getFreeTicket(payload, userToken): Observable<any> {
        const endpoint = this.baseUrl + "/events/getFreeTicket";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, payload, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            });
    }
}