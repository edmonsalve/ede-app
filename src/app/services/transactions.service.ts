import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from "rxjs/Rx";

import { environment as ENV } from '../../../src/environments/environment';

@Injectable()
export class TransactionsService {

    private baseUrl: string = ENV.BaseUrl;
    
    constructor(private http: Http) {}

    public getBanksList() {
        const pbKey = {
            "public_key": "471761c2792a4aeb334bab7e6094659d"
        }
        const endpoint = "https://secure.payco.co/restpagos/pse/bancos.json"

        return this.http.get(endpoint, { params: pbKey })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public createCreditCard(data, userToken) {
        const endpoint = this.baseUrl + "/payments/createCreditCard";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, data, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public getTransactionStatus(userId, invoice) {
        const endpoint = this.baseUrl + `/payments/transactionStatus?userId=${userId}&&invoice=${invoice}`;
        return this.http.get(endpoint)
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public buyTicketWithPSE(data, userToken): Observable<object> {
        const endpoint = this.baseUrl + "/payments/buyTicketPSE";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, data, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public buyTicketWithCreditCard(data, userToken): Observable<object> {
        const endpoint = this.baseUrl + "/payments/buyTicketCreditCard";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, data, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            })
    }

    public buyTicketWithCash(data, userToken): Observable<object> {
        const endpoint = this.baseUrl + "/payments/buyTicketCash";
        let headers: Headers = new Headers();
        headers.append('Authorization', `Bearer ${userToken}`);
        return this.http.post(endpoint, data, { headers })
            .map(res => res.json())
            .catch((e) => {
                return Observable.throw(e);
            });
    }
}