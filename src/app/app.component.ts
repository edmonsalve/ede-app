import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private store: Storage) {

    this.store.get("loggedUser").then((value) => {
      if (value && value.role == 'Portero') {
        this.rootPage = "TicketsValidationPage";
      } else if (this.platform.is('android') && !value) {
        this.rootPage = "LoginPage";
      } else {
        this.rootPage = "HomePage";
      }
      this.initializeApp();
    });
  }

  public initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (this.platform.is('ios')) {
        this.statusBar.styleDefault();
      }
      this.splashScreen.hide();
    });
  }
}

