import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrivacyDataTreatmentPage } from './privacy-data-treatment';

@NgModule({
  declarations: [
    PrivacyDataTreatmentPage,
  ],
  imports: [
    IonicPageModule.forChild(PrivacyDataTreatmentPage),
  ],
})
export class PrivacyDataTreatmentPageModule {}
