import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-privacy-data-treatment',
  templateUrl: 'privacy-data-treatment.html',
})
export class PrivacyDataTreatmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public goBack() {
    this.navCtrl.pop();
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivacyDataTreatmentPage');
  }

}
