import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyTicketsPage } from './my-tickets';
import { PipesModule } from '../../app/pipes/pipes.module';

@NgModule({
  declarations: [
    MyTicketsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyTicketsPage),
    PipesModule
  ],
})
export class MyTicketsPageModule {}
