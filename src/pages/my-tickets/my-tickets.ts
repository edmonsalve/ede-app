import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { EventsProvider } from '../../app/services/events.service';

import * as moment from "moment";

@IonicPage()
@Component({
  selector: 'page-my-tickets',
  templateUrl: 'my-tickets.html',
})
export class MyTicketsPage {

  public loggedUser: any;
  public myTickets: any[] = [];
  public showEvent: boolean = false;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    private _alertCtrl: AlertController,
    private _store: Storage,
    private _eventsService: EventsProvider
  ) {}

  async ionViewDidLoad() {
    this.myTickets = await this._store.get("my-tickets");
    this.showEvent = true;
    this.loggedUser = await this._store.get("loggedUser");
    this.havePendingTickets();
  }

  public deleteOldTickets() {
    this.myTickets = this.myTickets.filter((event) => moment(event.endDate).diff(new Date()) > 0);
    //this.store.set("list", this.events);
  }

  public async havePendingTickets() {
    let pendingTickets = await this._store.get('pending-tickets');
    if (pendingTickets && pendingTickets.length > 0) {
      pendingTickets.map((pTicket: any, index) => {
        this._eventsService.verifyPendingTicket(this.loggedUser['_id'], pTicket._id, this.loggedUser.token).subscribe(async (response) => {
          if (response.status == "success") {
            pTicket.qr = response.data.code;
            if (!this.myTickets) {
              this.myTickets = [];
            }
            this.myTickets.push(pTicket);
            delete pendingTickets[index];
            await this._store.set('pending-tickets', pendingTickets);
            await this._store.set("my-tickets", this.myTickets);
            let alert = this._alertCtrl.create({
              title: "En hora buena!",
              message: `Tu pago para el evento ${pTicket.name} ha sido aprobado, tu ticket quedará guardado en esta sección. Preparate para bailar!`,
              buttons: [{role: 'OK', text: 'OK'}]
            });
            alert.present();
          }
        });
      });
    }
  }

  public goBack() {
    this.navCtrl.pop();
  }

  public goToTicket(pEvent: any) {
    this.navCtrl.push('QrViewerPage', { event: pEvent });
  }

}
