import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthService } from '../../app/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-recovery-password',
  templateUrl: 'recovery-password.html',
})
export class RecoveryPasswordPage {

  public email: string = "";

  public emailForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService, private _alertCtrl: AlertController, private formBuilder: FormBuilder) {
    this.emailForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])]
    });
  }

  ionViewDidLoad() {
    
  }

  public recoveryPassword() {
    let email = this.email.trim();
    this.emailForm.controls.email.setValue(email);
    this.emailForm.updateValueAndValidity();
    if (this.emailForm.valid) {
      this.authService.recoveryPassword(this.email).subscribe((response) => {
        let alertAlreadyRegistered = this._alertCtrl.create({
          title: "Email enviado!",
          message: "Revisa tu correo, hemos enviado un mail con el link para tu cambio de clave",
          buttons: [{ text: "OK", handler: () => { this.navCtrl.pop() }}]
        });
        alertAlreadyRegistered.present();
      }, (error) => {
        let alertAlreadyRegistered = this._alertCtrl.create({
          title: "Oops!",
          message: "Este email no se encuentra registrado en EDE",
          buttons: ["OK"]
        });
        alertAlreadyRegistered.present();
      })
    } else if (this.emailForm.controls.email.errors.pattern) {
      let alertAlreadyRegistered = this._alertCtrl.create({
        title: "Oops!",
        message: "Por favor ingrese un email valido",
        buttons: ["OK"]
      });
      alertAlreadyRegistered.present();
    } else if (this.email == "") {
      let alertAlreadyRegistered = this._alertCtrl.create({
        title: "Oops!",
        message: "Ingrese su email",
        buttons: ["OK"]
      });
      alertAlreadyRegistered.present();
    }
  }

}
