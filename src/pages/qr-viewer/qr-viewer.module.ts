import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrViewerPage } from './qr-viewer';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [
    QrViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(QrViewerPage),
    NgxQRCodeModule
  ],
})
export class QrViewerPageModule {}
