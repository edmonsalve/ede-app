import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-qr-viewer',
  templateUrl: 'qr-viewer.html',
})
export class QrViewerPage {

  eventToShow: any;
  qrValue: string = null;

  public comeFromPayment: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidEnter() {
    moment.locale('es');
  }

  ionViewDidLoad() {
    this.eventToShow = this.navParams.get("event");
    this.comeFromPayment = this.navParams.get("comeFromPayment");
    this.eventToShow.showDate = `${moment(this.eventToShow.startDate).format('DD-MMMM-YYYY')} - ${moment(this.eventToShow.startDate).format('hh:mm')} PM`;
    this.qrValue = String(this.eventToShow.qr);
    if (this.comeFromPayment) {
      let alert = this.alertCtrl.create({
        title: "Transacción exitosa!",
        message: "Has adquirido tu ticket, quedará guardado en la sección 'Mis tickets'. Prepárate para bailar!",
        buttons: [{role: 'OK', text: 'OK'}]
      });
      alert.present();
    }
  }

  public goBack() {
    this.navCtrl.setRoot('HomePage');
  }

}
