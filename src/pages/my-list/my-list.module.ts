import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyListPage } from './my-list';
import { PipesModule } from '../../app/pipes/pipes.module';

@NgModule({
  declarations: [
    MyListPage,
  ],
  imports: [
    IonicPageModule.forChild(MyListPage),
    PipesModule
  ],
})
export class MyListPageModule {}
