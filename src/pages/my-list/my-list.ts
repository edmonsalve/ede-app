import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Storage } from "@ionic/storage";
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-my-list',
  templateUrl: 'my-list.html',
})
export class MyListPage {

  public currentEvents: any[];
  public events: any[] = [];

  public showEvent: boolean = false;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    private store: Storage,
    private _alertCtrl: AlertController
  ) {}

  async ionViewDidLoad() {
    this.events = await this.store.get("list");
    this.deleteOldEvents();
    this.currentEvents = await this.navParams.get("currentEvents");
    this.showEvent = true;
  }

  public goToEventDetail(event: object) {
    event['onMyList'] = true
    this.navCtrl.push("EventDetailPage", { event })
  }

  public goToCheckout(event: object) {
    this.navCtrl.push("CheckoutPage", { event });
  }

  public deleteOldEvents() {
    if (this.events) {
      this.events = this.events.filter((event) => moment(event.endDate).diff(new Date()) > 0);
    }
    //this.store.set("list", this.events);
  }

  public removeFromList(evt: any): any {
    let alert = this._alertCtrl.create({
      title: "Se eliminó el evento de tu lista",
      buttons: ["OK"]
    });
    this.events = this.events.filter(event => event._id !== evt._id);
    this.store.set('list', this.events);
  }

  public goToQrViewer(event) {
    this.navCtrl.push("QrViewerPage", { event })
  }

  public goBack() {
    this.navCtrl.pop();
  }

}
