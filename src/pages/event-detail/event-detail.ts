import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EventsProvider } from '../../app/services/events.service';
import { Storage } from "@ionic/storage";
import * as moment from 'moment';


@IonicPage()
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html',
  animations: [
  ]
})
export class EventDetailPage {

  public viewMoreState: string = 'closed';

  public loggedUser: any;
  public eventToShow: any = {};
  public haveATicket: boolean = false;
  public inGuestList: boolean = false;


  /** Animation Tools and vbles */
  private cardHeight: String = '50%';
  private eventInfoHeight: String = '34%';
  private actionsContainerHeight: String = '16%';
  private ticketHeight: String = '50%';
  private beneficiosHeight: String = '50%';
  public titleSize: string = '1.3em';
  public showActions: Boolean = true;
  public showDescription: Boolean = true;
  public showBenefits: Boolean = false;
  public showRotateLike: boolean = false;
  public ticketOpened = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    private inAppBrowser: InAppBrowser,
    private eventsService: EventsProvider,
    private storage: Storage,
    private alertCtrl: AlertController,
    private loaderCtrl: LoadingController
  ) { }

  async ionViewDidLoad() {
    moment.locale('es');
    this.eventToShow = await this.navParams.get("event");
    this.eventToShow.showDate = `${moment(this.eventToShow.startDate).format('DD-MMMM-YYYY')} - ${this.converTime(this.eventToShow.startTime)} A ${this.converTime(this.eventToShow.endTime)}`;
    this.loggedUser = await this.storage.get('loggedUser');
    this.haveThisTicket();
  }

  private async haveThisTicket() {
    let tickets: object[] = await this.storage.get('my-tickets');
    if (!tickets) {
      return false;
    }
    let have = false;
    tickets.map((event) => {
      if(event['_id'] === this.eventToShow['_id']) {
        this.eventToShow['qr'] = event['qr'];
        have = true;
      }
    });
    return have;
  }

  private async imOnTheGuestlist() {
    let guestlist: object[] = await this.storage.get('guestlist');
    if (!guestlist) {
      return false;
    }
    let have = false;
    guestlist.map((event) => {
      if(event['_id'] === this.eventToShow['_id']) {
        this.eventToShow['qr'] = event['qr'];
        have = true;
      }
    });
    return have;
  }

  public getSafeImage(urlToSanitize: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(urlToSanitize);
  }

  public async addToMyList() {
    if (this.canInteract()) {
      this.showRotateLike = true;
      setTimeout(() => {
        this.showRotateLike = false;
        let alertAddToList = this.alertCtrl.create({
          title: "Se agregó el evento a tu lista",
          buttons: ["OK"]
        });
        alertAddToList.present();
      }, 400)
      let myList = await this.storage.get("list");
      if (!myList) {
        myList = [];
      }
      myList.push(this.eventToShow);
      this.eventToShow.onMyList = true;
      await this.storage.set("list", myList);
    } else {
      
    }
  }

  public async removeFromList(id: string) {
    let myList = await this.storage.get("list");
    myList = myList.filter(event => event._id !== this.eventToShow['_id']);
    this.storage.set('list', myList);
    let alertRemove = this.alertCtrl.create({
      title: "Se eliminó el evento de tu lista",
      buttons: ["OK"]
    });
    alertRemove.present();
    this.eventToShow.onMyList = false;
  }

  public async addToMyGuestList() {
    let myList = await this.storage.get("guestlist");
    if (!myList) {
      myList = [];
    }
    myList.push(this.eventToShow);
    await this.storage.set("guestlist", myList);
  }

  public requestGuestlistInfo() {
    let alertRequest = this.alertCtrl.create({
      buttons: [{
        text: 'OK',
        handler: (data) => {
          this.subscribeToGuestlist(data);
        }
      }],
      inputs: [
        {
          name: 'name',
          placeholder: 'Nombre'
        },
        {
          name: 'document',
          placeholder: 'Cedula',
          type: 'number'
        },
        {
          name: 'cellphone',
          placeholder: 'Número celular',
          type: 'number'
        },
        {
          name: 'email',
          placeholder: 'Email',
          type: 'text'
        }
      ],
      title: "Datos para la lista",
      message: "Ingrese los datos para inscribir a la lista"
    });
    alertRequest.present();
  }

  public requestFreeTicketData() {
    if (this.canInteract()) {
      let alertRequest = this.alertCtrl.create({
        buttons: [{
          text: 'OK',
          handler: (data) => {
            this.getFreeTicket(data);
          }
        }],
        inputs: [
          {
            name: 'cellphone',
            placeholder: 'Número celular',
            type: 'number'
          },
        ],
        title: "Datos para el ticket",
        message: "Ingresa los datos para terminar el registro"
      });
      alertRequest.present();
    }
  }

  private async getFreeTicket(data) {
    if (data.cellphone && data.cellphone.length == 10) {
      let loader = this.loaderCtrl.create({
        spinner: 'hide',
        content: `<div class="ede-loader">
                    <img src="assets/gifs/ede-loader.gif">
                    <span>Estamos generando tu entrada...</span>
                  </div>`
      });
      loader.present();
      let payload = {
        userId: this.loggedUser._id,
        eventId: this.eventToShow._id,
        questions: {
          cellphone: data.cellphone
        }
      }
      this.eventsService.getFreeTicket(payload, this.loggedUser.token).subscribe(async (response) => {
        if (response.status == "success") {
          let myTickets = await this.storage.get("my-tickets");
          if (!myTickets) {
            myTickets = [];
          }
          this.eventToShow.qr = response.qr;
          myTickets.push(this.eventToShow);
          await this.storage.set("my-tickets", myTickets);
          loader.dismiss();
          this.navCtrl.push('QrViewerPage', { event: this.eventToShow });
        } else {
          loader.dismiss();
        }
      });
    } else {
      let alert = this.alertCtrl.create({message: 'Su número celular debe tener 10 digitos', buttons: ['OK']});
      alert.present();
    }
  }

  private subscribeToGuestlist(data: any) {
    if (data.name && data.document && data.cellphone && data.email) {
      if (data.cellphone.length == 10) {
        let loader = this.loaderCtrl.create({
          spinner: 'hide',
          content: `<div class="ede-loader">
                      <img src="assets/gifs/ede-loader.gif">
                      <span>Te estamos inscribiendo a la lista de invitados...</span>
                    </div>`
        });
        loader.present();
        this.eventsService.subscribeToGuestlist(data.name, data.document, data.cellphone, data.email, this.eventToShow._id, this.loggedUser.token).subscribe((response) => {
          if (response.status == "success") {
            loader.dismiss();
            let alertSubscribed = this.alertCtrl.create({
              title: "¡Estás en la lista!",
              message: "¡Ya estás inscrito! Ahora puedes asistir al evento, recuerda llevar tu cedula para comprobar tu identidad en la entrada.",
              buttons: ["OK"]
            });
            alertSubscribed.present();
          } else if (response.status == "already registered") {
            let alertAlready = this.alertCtrl.create({
              title: "Esta cedula ya esta registrada en el guestlist.",
              buttons: ["OK"]
            });
            alertAlready.present();
            loader.dismiss();
          } else {
            let alert = this.alertCtrl.create({
              title: "Oops!",
              message: response.message,
              buttons: ["OK"]
            });
            alert.present();
            loader.dismiss();
          }
        })
      } else {
        let alert = this.alertCtrl.create({message: 'Su número celular debe tener 10 digitos', buttons: ['OK']});
        alert.present();
      }
    } else {
      let alert = this.alertCtrl.create({message: 'Debe ingresar los datos.', buttons: ['OK']});
      alert.present();
    }
  }

  public showAlertForFriendTicket() {
    let alert = this.alertCtrl.create({message: 'Pronto podrás comprar y enviarle tickets a tus amigos', buttons: ['Entendido']});
    alert.present();
  }

  public showPromotionalAlert() {
    let alert = this.alertCtrl.create({message: 'Este evento no dispone de boletería en este medio', buttons: ['Entendido']});
    alert.present();
  }

  public converTime(time) {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join('').replace('AM', ' AM').replace('PM', ' PM');
  }

  public goBack() {
    this.navCtrl.pop();
  }

  public goToQrPage() {
    this.navCtrl.push('QrViewerPage', { event: this.eventToShow });
  }

  public shareEvent() { }

  private canInteract() {
    if (this.loggedUser != null && this.loggedUser != undefined) {
      return true;
    } else {
      let alertLogin = this.alertCtrl.create({
        title: "Debe iniciar sesión o registrarse para interactuar con EDE",
        buttons: ["OK"]
      });
      alertLogin.present();
      return false;
    }
  }

  public openBenefits() {
    if (this.canInteract()) {
      this.showActions = false;
      this.showDescription = false;
      this.showBenefits = true;
      this.cardHeight = '10%';
      this.eventInfoHeight = '10%';
      this.actionsContainerHeight = '80%';
      this.ticketHeight = '10%';
      this.beneficiosHeight = '90%';
      this.ticketOpened = false;
      this.titleSize = '13px';
    }
  }

  public resetView() {
    this.showActions = true;
    this.showDescription = true;
    this.showBenefits = false;
    this.cardHeight = '50%';
    this.eventInfoHeight = '34%';
    this.actionsContainerHeight = '16%';
    this.ticketHeight = '50%';
    this.beneficiosHeight = '50%';
    this.titleSize = '1.3em';
    this.ticketOpened = false;
  }

  public openTicket() {
    if (this.canInteract()) {
      this.showActions = false;
      this.showDescription = false;
      this.showBenefits = false;
      this.ticketOpened = true;
      this.cardHeight = '10%';
      this.eventInfoHeight = '10%';
      this.actionsContainerHeight = '80%';
      this.ticketHeight = '90%';
      this.beneficiosHeight = '10%';
      this.titleSize = '13px';
    }
  }

  public openInfo() {
    if (this.viewMoreState == 'closed') {
      this.viewMoreState = 'open';
      this.showActions = false;
      this.showDescription = true;
      this.showBenefits = false;
      this.ticketOpened = false;
      this.cardHeight = '10%';
      this.eventInfoHeight = '74%';
      this.actionsContainerHeight = '16%';
      this.ticketHeight = '50%';
      this.beneficiosHeight = '50%';
      this.titleSize = '1.3em';
    } else {
      this.viewMoreState = 'closed';
      this.resetView();
    }
  }

  public formatDate(date: string) {
    return moment(date).format('DD MMMM');
  }

  public goToCheckout() {
    this.navCtrl.push('CheckoutPage', { event: this.eventToShow });
  }

}
