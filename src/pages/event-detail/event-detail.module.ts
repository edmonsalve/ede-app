import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventDetailPage } from './event-detail';

import { PipesModule } from './../../app/pipes/pipes.module';
import { IonicStorageModule } from "@ionic/Storage";

@NgModule({
  declarations: [
    EventDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(EventDetailPage),
    PipesModule
  ],
  providers: [
  ]
})
export class EventDetailPageModule {}
