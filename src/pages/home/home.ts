import { Component, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController, IonicPage, Slides, AlertCmp, AlertController, Refresher } from 'ionic-angular';

import { EventsProvider } from '../../app/services/events.service';
import { CalendarComponentOptions } from 'ion2-calendar';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AuthService } from "../../app/services/auth.service";

@IonicPage()
@Component({
    selector: 'ede-home',
    templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Slides) slides: Slides;
  dateRange: { from: string; to: string; };
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsRange: CalendarComponentOptions = {
      pickMode: 'range'
  };
  public fromDateFilter: string;
  public endDateFilter: string;
  public selectedCity = 'Todas';
  public selectedGender = 'Todos';


  public occAnniversary = {
    "_id" : "5d67d1cf7b54e87d6fa52462",
    "promoters" : [],
    "tickets" : [ 
      {
          "name" : "Lanzamiento (50 unidades)",
          "price" : 40000,
          "amount_available" : 50,
          "start_date" : "2019-08-29T05:00:00.000Z",
          "end_date" : "2019-09-14T05:00:00.000Z",
          "amount_sold" : 0
      }, 
      {
          "name" : "Etapa 2 (50 unidades)",
          "price" : 50000,
          "amount_available" : 50,
          "start_date" : "2019-09-15T05:00:00.000Z",
          "end_date" : "2019-10-20T05:00:00.000Z",
          "amount_sold" : 0
      }
    ],
    "validators" : [],
    "name" : "7° Aniversario: Oriente's Crowd Pres: Kwartz ",
    "musicalGender" : "Techno",
    "eventType" : "payment",
    "description" : "7 años de sembrar semillas encontrándonos con la mente de miles de personas a través de un lenguaje analfabeta pero tan poderoso como el sonido. 7 años de conectar con el entorno y medio ambiente hasta transformar nuestro territorio. OCC los invita a celebrar 7 años de resistencia y a brindar en nombre de que sean muchísimos más. ",
    "lineUp" : "Kwartz [Pole group]",
    "imageSrc" : "https://firebasestorage.googleapis.com/v0/b/ede-lab.appspot.com/o/test%2F1567085004791_k.jpg?alt=media&token=54a61cd2-b2fd-4a57-a463-2db197cdd0d7",
    "location" : {
        "city" : "Oriente Antioqueño",
        "address" : "Por confirmar"
    },
    "startDate" : "2019-10-19T05:00:00.000Z",
    "endDate" : "2019-10-20T05:00:00.000Z",
    "startTime" : "21:00",
    "endTime" : "04:00",
    "guestlist" : 1,
    "guestlistQuantity" : 50000,
    "benefits" : "",
    "warnings" : "",
    "status" : "active",
    "signature" : "Oriente's Crowd Community",
    "vjLineUp" : "",
    "createdDate" : "Thu Aug 29 2019 08:23:24 GMT-0500 (hora estándar de Colombia)",
    "userId" : "5d1bc1db76e6fa738969c9d6",
  }

  public cities: string[] = [
      'Medellín',
      'Bogotá',
      'Cali',
      'Manizales',
      'Mute Medellín',
      'Bucaramanga',
      'Calle 9 + 1',
      'Pereira',
      'Popayán',
      'Barranquilla',
      'Armenia',
      'Bello',
      'Tolú',
      'Oriente Antioqueño',
      'Santa Elena'
  ];

  public musicalGenders: string[] = [
    'House',
    'Minimal',
    'Techno',
    'Tech House',
    'Acid',
    'Ambient',
    'Trance',
    'Drum n bass',
    'Festival',
    'Deep house',
    'Dubstep',
    'Hardstyle',
    'Progressive House',
    'Progressive Trance',
    'Electro'
  ];

  
  public events: any[] = [];
  public copyEvents: any[] = [];
  public showEvent: boolean = false;

  public showUserMenu = false;
  public showAppMenu = false;
  public settupWidth = "-100%";
  public musicalGenderWidth = "0%";
  public cityWidth = "0%";
  public dateWidth = "0%";
  public topEvents: any[] = [];
  private currentSlide: number = 0;

  public addedToList: boolean = false;
  public showRotateLike: boolean = false;
  public initialList: any;
  public initialTickets: any;

  public loggedUser: any;

  constructor(
      public navCtrl: NavController,
      private eventsService: EventsProvider,
      private sanitizer: DomSanitizer,
      private storage: Storage,
      private _alertCtrl: AlertController,
      private _inAppBrowser: InAppBrowser,
      private _authService: AuthService
  ) { }

  public async ionViewDidLoad() {
    this.musicalGenders.sort();
    this.cities.sort();
    this.loggedUser = await this.storage.get("loggedUser");
    this.eventsService.getEvents()
        .subscribe((response: any) => {
          response.data.sort((left, right) => {
            return moment.utc(left.startDate).diff(moment.utc(right.startDate));
          });
          this.events = response.data;
          this.copyEvents = response.data;
          this.topEvents = this.events.slice(0, 3);
          this.checkMyList();
          this.checkMyTickets();
        });
  }

  public filterByName(text: any) {
    let result = [];
    let value = text.srcElement.value;
    if (this.events.length == 0) this.events = this.copyEvents;
    if (value && value.length > 2) {
        this.events.map((eventMapped) => {
            if (
              eventMapped.name.toLowerCase().includes(value.toLowerCase()) || 
              eventMapped.description.toLowerCase().includes(value.toLowerCase()) ||
              eventMapped.location.city.toLowerCase().includes(value.toLowerCase()) ||
              eventMapped.lineUp.toLowerCase().includes(value.toLowerCase()) ||
              eventMapped.location.address.toLowerCase().includes(value.toLowerCase())) {
                result.push(eventMapped);
            }
        });
        this.events = result;
    } else if (!value || value.length == 0) {
        this.events = this.copyEvents;
    }
  }

  public filterEvents() {
    let result = [];
    if (this.events.length == 0) {
        this.events = this.copyEvents;
    }
    this.events.map((eventMapped) => {
        // Busqueda por todos los campos
      if (this.selectedCity != 'Todas' && this.selectedGender != 'Todos' && (this.dateRange)) {
          if (eventMapped.location.city.includes(this.selectedCity) && eventMapped.musicalGender == this.selectedGender) {

              if (moment(eventMapped.startDate).isBetween(this.dateRange.from, this.dateRange.to) || moment(eventMapped.startDate).isSame(this.dateRange.from)) {
                  result.push(eventMapped);
              }
          }
      // Busqueda por ciudad y fecha
      } else if (this.selectedCity != 'Todas' && this.selectedGender == 'Todos' && (this.dateRange)) {
          if (eventMapped.location.city.includes(this.selectedCity)) {

              if (moment(eventMapped.startDate).isBetween(this.dateRange.from, this.dateRange.to) || moment(eventMapped.startDate).isSame(this.dateRange.from)) {
                  result.push(eventMapped);
              }
          }
      // Busqueda por genero musical y ciudad
      } else if (this.selectedCity != 'Todas' && this.selectedGender != 'Todos' && (!this.dateRange)) {
          if (eventMapped.location.city.includes(this.selectedCity) && eventMapped.musicalGender == this.selectedGender) {
              result.push(eventMapped);
          }
      // Busqueda por genero musical y fecha
      } else if (this.selectedCity == 'Todas' && this.selectedGender != 'Todos' && (this.dateRange)) {
          if (eventMapped.musicalGender == this.selectedGender) {

              if (moment(eventMapped.startDate).isBetween(this.dateRange.from, this.dateRange.to) || moment(eventMapped.startDate).isSame(this.dateRange.from)) {
                  result.push(eventMapped);
              }
          }
      // Busqueda solo por fecha
      } else if (this.selectedCity == 'Todas' && this.selectedGender == 'Todos' && (this.dateRange)) {

          if (moment(eventMapped.startDate).isBetween(this.dateRange.from, this.dateRange.to) || moment(eventMapped.startDate).isSame(this.dateRange.from)) {
              result.push(eventMapped);
          }
      // Busqueda solo por ciudad
      } else if (this.selectedCity != 'Todas' && this.selectedGender == 'Todos' && (!this.dateRange)) {
          if (eventMapped.location.city.includes(this.selectedCity)) {
              result.push(eventMapped);
          }
      // Busqueda solo por genero
      } else if (this.selectedCity == 'Todas' && this.selectedGender != 'Todos' && (!this.dateRange)) {
          if (eventMapped.musicalGender == this.selectedGender) {
              result.push(eventMapped);
          }
      } 
    });
    this.events = result;
    if (this.selectedCity == 'Todas' && this.selectedGender == 'Todos' && (!this.dateRange)) {
        this.events = this.copyEvents;
    }
  }

  public doRefresh(emiter: Refresher) {

    this.eventsService.getEvents()
        .subscribe((response: any) => {
          response.data.sort((left, right) => {
            return moment.utc(left.startDate).diff(moment.utc(right.startDate));
          });
          this.events = response.data;
          this.copyEvents = response.data;
          this.topEvents = this.events.slice(0, 3);
          this.refreshFilters();
          this.checkMyList();
          this.checkMyTickets();
          setTimeout(() => {
            emiter.complete();
          }, 2000)
        });
  }

  public async addToMyList(event) {
    if (this.canInteract()) {
      event['showRotateLike'] = true;
      setTimeout(() => {
        event['showRotateLike'] = false;
        this.events.map((eventMapped) => {
          if (eventMapped['_id'] == event['_id']) {
            eventMapped['onMyList'] = true;
          }
        })
        let alert = this._alertCtrl.create({
          title: "Se agrego el evento a tu lista",
          buttons: ["OK"]
        });
        alert.present();
      }, 400)
      let myList = await this.storage.get("list");
      if (!myList) {
        myList = [];
      }
      myList.push(event);
      await this.storage.set("list", myList);
    }
  }

  public async removeFromList(event: any) {
    let myList = await this.storage.get("list");
    myList = myList.filter(eventMapped => eventMapped._id !== event._id);
    this.storage.set('list', myList);
    this.events.map((eventMapped) => {
      if (eventMapped['_id'] == event._id) {
        eventMapped['onMyList'] = false;
      }
    })
    /*let alert = this._alertCtrl.create({
      title: "Se eliminó el evento de tu lista",
      buttons: ["OK"]
    });
    alert.present();*/
  }

  public async checkMyList() {
    let myList = await this.storage.get("list");
    if (myList) {
      myList.map((eventMapped) => {
        this.events.map((event) => {
          if (eventMapped._id == event._id) event['onMyList'] = true;
        });
      });
    }
  }

  public async checkMyTickets() {
    let myTickets = await this.storage.get("list");
    if (myTickets) {
      myTickets.map((eventMapped) => {
        this.events.map((event) => {
          if (eventMapped._id == event._id) {
            event['qr'] = eventMapped.qr;
          }
        });
      });
    }
  }

  private canInteract() {
    if (this.loggedUser != null && this.loggedUser != undefined) {
      return true;
    } else {
      let alertLogin = this._alertCtrl.create({
        title: "Debe iniciar sesión o registrarse para interactuar con EDE",
        buttons: ["OK"]
      });
      alertLogin.present();
      return false;
    }
  }

  public swipped(event) {
    setTimeout(() => {
      console.log('Async operation has ended');
    }, 2000);
  }

  public clearDates() {
    this.dateRange = undefined;
    this.filterEvents();
  }

  public assignDateFilters() {
    this.fromDateFilter = moment(this.dateRange.from).format('DD-MM');
    let end = moment(this.dateRange.to).format('DD-MM');
    if (this.fromDateFilter != end) {
      this.endDateFilter = end;
    }
    this.closeMenus();
    this.filterEvents();
  }

  private refreshFilters() {
    this.clearDates();
    this.selectedCity = "Todas";
    this.selectedGender = "Todos";
  }

  public goToPrev() {
    this.slides.slidePrev(500);
  }

  public goToLogin() {
    this.navCtrl.push('LoginPage');
  }

  public goToCheckout(event) {
    if (this.canInteract()) {
      this.navCtrl.push('CheckoutPage', { event });
    }
  }

  public goToQrViewer(event) {
    this.navCtrl.push('QrViewerPage', { event });
  }

  public goToWebAdmin() {
    const admin = this._inAppBrowser.create("https://edance-lab.com/signup");
  }

  public goToFAQ() {
    const admin = this._inAppBrowser.create("https://edance-lab.com/signup");
    
  }

  public toggleUserMenu() {
    this.showUserMenu = !this.showUserMenu;
  }

  public toggleAppMenu() {
    this.showAppMenu = !this.showAppMenu;
  }

  public goToMyTickets() {
    this.navCtrl.push('MyTicketsPage');
  }

  public settupMenu() {
    if (this.settupWidth == "-1%") {
        this.settupWidth = "-98%";
    } else {
        this.cityWidth = "0%";
        this.musicalGenderWidth = "0%";
        this.dateWidth = "0%";
        this.settupWidth = "-1%";
    }
  }

  public dateMenu() {
    if (this.dateWidth == "0%") {
        this.settupWidth = "-100%";
        this.cityWidth = "0%";
        this.musicalGenderWidth = "0%";
        this.dateWidth = "98%";
    } else {
        this.dateWidth = "0%";
    }
  }

  public cityMenu() {
    if (this.cityWidth == "0%") {
        this.dateWidth = "0%";
        this.settupWidth = "-100%";
        this.musicalGenderWidth = "0%";
        this.cityWidth = "98%";
    } else {
        this.cityWidth = "0%";
    }
  }

  public musicalGenderMenu() {
    if (this.musicalGenderWidth == "0%") {
        this.dateWidth = "0%";
        this.settupWidth = "-100%";
        this.cityWidth = "0";
        this.musicalGenderWidth = "98%";
    } else {
        this.musicalGenderWidth = "0%";
    }
  }

  public closeMenus() {
    this.musicalGenderWidth = "0%";
    this.settupWidth = "-100%";
    this.cityWidth = "0%";
    this.dateWidth = "0%";
  }

  public goToNext(number) {
    this.slides.slideNext(500);
  }

  public goToEventDetail(event: object) {
    this.navCtrl.push("EventDetailPage", { event })
  }

  public goToMyList() {
    this.navCtrl.push("MyListPage", { currentTickets: this.initialTickets });
  }

  public goToTerminos() {
    this.navCtrl.push("TerminosPage");
  }

  public goToAbout() {
    const admin = this._inAppBrowser.create("http://www.edancevent.com/servicios/");
  }

  public async logout() {
    await this.storage.remove('loggedUser');
    this.navCtrl.setRoot('LoginPage');
  }

  public isLogged(): boolean {
    return this.loggedUser != null && this.loggedUser != undefined ? true: false;
  }


}
