import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PipesModule } from '../../app/pipes/pipes.module';
import { CalendarModule } from "ion2-calendar";
import { NgxQRCodeModule } from 'ngx-qrcode2';


@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    PipesModule,
    CalendarModule,
    NgxQRCodeModule
  ],
})
export class EventDetailPageModule {}