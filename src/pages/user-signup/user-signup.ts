import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';
import User from '../../app/models/user.model';
import { AuthService } from '../../app/services/auth.service';
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: 'user-signup',
  templateUrl: 'user-signup.html',
})
export class UserSignupPage {
  public user: User = new User();
  public confirmPassword: string = "";
  public termsChecked = false;
  public privacyChecked = false;

  public ages: Array<number> = [];
  public gender: string = "Femenino"
  public cities: string[]  = [
    'Bogotá', 'Cali', 'Barranquilla', 'Bucaramanga', 'Cartagena', 'Santa Marta',
    'Cúcuta', 'Pereira', 'Manizales', 'Ibagué', 'Pasto', 'Villavicencio', 'Valledupar',
    'Armenia', 'Popayán', 'Tunja', 'Buenaventura', 'Rioacha', 'Soledad',
    'Quibdó', 'Sincelejo', 'Florencia', 'Soacha', 'Yopal', 'Barrancabermeja', 'Leticia',
    'Bello', 'Ipiales', 'Sogamoso', 'Buga', 'Itagüí', 'Girardot', 'Yumbo', 'Zipaquirá', 'Floridablanca',
    'Puerto Carreño', 'Mocoa', 'Tumaco', 'Envigado', 'Tuluá', 'Cartago', 'San Andrés', 'Santa Elena',
    'Medellín', 'Buga', 'Guarne', 'Apartadó', 'Ciénaga', 'Facatativá', 'Montería', 'Neiva',
    'Tunja', 'Palmira'
  ];

  constructor (
    public navCtrl: NavController, 
    public navParams: NavParams,
    private _authService: AuthService,
    private _loaderCtrl: LoadingController,
    private _alertCtrl: AlertController,
    private _storage: Storage,
    private _platform: Platform
  ) { }

  ionViewWillEnter(){
    for (let i=18; i <= 70; i++) {
      this.ages.push(i);
    }
    this.cities.sort();
  }

  ionViewDidLoad() {
  }

  public signUp() {
    if (this.isValidForm()) {
      if (this.termsChecked && this.privacyChecked) {
        let loader = this._loaderCtrl.create({
          spinner: 'hide',
          content: `
            <div class="ede-loader">
              <img src="assets/gifs/ede-loader.gif">
              <span>Creando el usuario</span>
            </div>
          `,
        });
        let alert = this._alertCtrl.create({title: "Error", message: "Las contraseñas no coinciden", buttons: ["OK"] });
        this.user.signature = "";
        this.user.role = "Raver";
        this.user.email = this.user.email.toLowerCase();
        this.user.email = this.user.email.trim();
        if (this.user.password === this.confirmPassword) {
          loader.present();
          this.user.status = 2;
          this._authService.signUp(this.user).subscribe(async (response) => {
            loader.dismiss();
            if (response.status == "success") {
              await this._storage.set('loggedUser', response.data);
              if (response.data.role == 'Portero') {
                this.navCtrl.setRoot('TicketsValidationPage');
              } else {
                this.navCtrl.setRoot('HomePage');
              }
            } else {
              let alertAlreadyRegistered = this._alertCtrl.create({
                title: "Oops!",
                message: "Este email ya se encuentra registrado en EDE",
                buttons: ["OK"]
              });
              alertAlreadyRegistered.present();
            }
          }, (error) => {
            loader.dismiss();
            let alertError = this._alertCtrl.create({
              title: "Oops!",
              message: "Ocurrio un error creando el usuario, intente de nuevo",
              buttons: ["Cerrar"]
            });
            alertError.present();
          })
        } else {
          alert.present();
        }
      } else {
        let alertTerms = this._alertCtrl.create({
          title: "Importante:",
          message: "Debes aceptar los términos y condiciones y la política de privacidad y tratamiento de datos.",
          buttons: ['OK']
        });
        alertTerms.present();
      }
    } else {
      let alertData = this._alertCtrl.create({
        title: "Cuidado!", 
        message: "Por favor complete todos los campos.",
        buttons: ['Entendido']
      });
      alertData.present();
    }
  }

  private isValidForm() {
    if (this.user.name != "" && this.user.email != "" && this.user.lastname != "" &&
      this.user.password != "") {
        
        return true;

    } else {
      return false;
    }
  }

  public goToTerminos() {
    this.navCtrl.push("TerminosPage");
  }

  public goToPrivacyDataTreatment() {
    this.navCtrl.push("PrivacyDataTreatmentPage");
  }

  public isIos() {
    return this._platform.is('ios');
  }

}
