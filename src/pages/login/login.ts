import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { UserSignupPage } from "../user-signup/user-signup";

import { AuthService } from '../../app/services/auth.service';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'ede-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public username: string = "";
  public password: string;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthService,
    private _alertCtrl: AlertController,
    private _loaderCtrl: LoadingController,
    private _storage: Storage
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public goToSignUp (): void {
    this.navCtrl.push(UserSignupPage);
  }

  public goToRecovery (): void {
    this.navCtrl.push('RecoveryPasswordPage');
  }

  public async login () {
    let loader = this._loaderCtrl.create({
      spinner: 'hide',
      content: `
        <div class="ede-loader">
          <img src="assets/gifs/ede-loader.gif">
          <span>Validando el usuario</span>
        </div>
      `,
    });
    let alert = this._alertCtrl.create({ title: "Error", message: "Verifique los datos ingresados", buttons: ["OK"]});
    if (this.username !== "" && this.password != "") {
      loader.present();
      this.username = this.username.toLowerCase();
      this.username = this.username.trim();
      this.authService.login(this.username, this.password)
          .subscribe(async (response: any) => {
            await this._storage.set('loggedUser', response.data);
            loader.dismiss();
            if (response.data.role == 'Portero') {
              this.navCtrl.setRoot('TicketsValidationPage');
            } else {
              this.navCtrl.setRoot('HomePage');
            }
          }, (error) => {
            console.error(error);
            loader.dismiss();
            alert.present();
          });
    }
  }

}
