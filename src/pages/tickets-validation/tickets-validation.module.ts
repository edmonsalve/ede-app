import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketsValidationPage } from './tickets-validation';

@NgModule({
  declarations: [
    TicketsValidationPage,
  ],
  imports: [
    IonicPageModule.forChild(TicketsValidationPage),
  ],
})
export class TicketsValidationPageModule {}
