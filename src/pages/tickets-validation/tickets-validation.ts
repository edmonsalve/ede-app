import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { EventsProvider } from '../../app/services/events.service';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-tickets-validation',
  templateUrl: 'tickets-validation.html',
})
export class TicketsValidationPage {

  public scanned: any;
  private loggedUser: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _scanner: BarcodeScanner,
    private _eventsProvider: EventsProvider,
    private _alertCtrl: AlertController,
    private _storage: Storage
  ) {}

  async ionViewDidLoad() {
    console.log('ionViewDidLoad TicketsValidationPage');
    this.loggedUser = await this._storage.get('loggedUser');
  }

  public openScanner() {
    let alertError = this._alertCtrl.create({ title: "Error", message: "El ticket ya fue redimido o no existe", buttons: ["OK"] });
    let alertSuccess = this._alertCtrl.create({ title: "Validación exitosa", message: "El ticket fue redimido correctamente, ingreso aprobado", buttons: ["OK"] });
    this._scanner.scan().then((result) => {
      let code = result.text;
      this._eventsProvider.validateTicket({ code }, this.loggedUser.token).subscribe((response: any) => {
        if (response.status == "fail") {
          alertError.present();
        } else {
          alertSuccess.present();
        }
      })
    }).catch((error) => {
      console.log(error);
    })
  }

  public showCellphone() {
    let alertAssistance = this._alertCtrl.create({ 
      title: "¿Tienes problemas validando los tickets?",
      message: "Comunícate al número celular: 300-365-1976 para recibir asistencia técnica.",
      buttons: ["Entendido"] 
    });
    alertAssistance.present();
  }

  public async logout() {
    await this._storage.remove('loggedUser');
    this.navCtrl.setRoot('LoginPage');
  }

}
