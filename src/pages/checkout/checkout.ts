import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { TransactionsService } from '../../app/services/transactions.service';

import * as moment from 'moment';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Cordova } from '@ionic-native/core';

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  public userDocument: string = "";
  public userDocumentType: string = "";
  public userCellPhone: string = "";

  public totalPrice: number = 0;
  public ticketsQuantity: number = 1;
  public TICKET_SERVICE: number;

  public currentTicketStage;

  public loggedUser: any;

  public banksList: Array<{ bankCode: string, bankName: string }> = [];
  public bankName: string = "";
  public cashType: string = "";

  public eventToBuy: any = {};


  public cardNumber: string;
  public cardCvc: string;
  public cardExpiration: string = "";
  public cardName: string = "";
  public cardLastname: string = "";

  public creditCardSelected: any;
  public addNewCard: boolean = false;

  public paymentMethod: string = "";
  public placeholders: any = {number: '•••• •••• •••• ••••', name: 'Full Name', expiry: '••/••', cvc: '•••'};
  public masks: any = {
      number: [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
      expiry: [/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
  };
  public messages: any = {validDate: 'valid\ndate', monthYear: 'mm/yyyy'};

  public availableCreditCards: object[];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _loaderCtrl: LoadingController,
    private _alertCtrl: AlertController,
    private _iab: InAppBrowser,
    private _storage: Storage,
    private _platform: Platform,
    private _transactionService: TransactionsService
  ) {}

  async ionViewDidLoad() {
    console.log(this._platform.is('ios'), 'is IOSS')
    this.eventToBuy = this.navParams.get('event');
    this.loggedUser = await this._storage.get('loggedUser');
    this.availableCreditCards = await this._storage.get('my-credit-cards');
    this._transactionService.getBanksList().subscribe((response) => {
      if (response.success) {
        this.banksList = response.data;
      }
    });
    this.currentTicketStage = this.getCurrentTicketsStage();
    if (this.currentTicketStage) {
      this.TICKET_SERVICE = Number(this.currentTicketStage.price) > 25000 ? 4500 : 2500;
      this.totalPrice = Number(this.currentTicketStage.price) + this.TICKET_SERVICE;
    } else {
      this.totalPrice = 0;
      let alert = this._alertCtrl.create({ 
        title: 'No hay boletería disponible',
        message: 
        `En este momento no hay tickets disponibles, revisa la boleteria en la descripción del evento`,
        buttons: [{role: 'OK', text: 'Entendido', handler: () => {this.navCtrl.pop()}}]
      });
      alert.present();
    }
  }

  public goBack() {
    this.navCtrl.pop();
  }

  public recalculatePrice() {
    this.totalPrice = (Number(this.currentTicketStage.price) + this.TICKET_SERVICE) * this.ticketsQuantity;
  }

  public checkIfCC(type: string) {
  }

  public async validateCard() : Promise<any> {
    
  }

  public async saveCreditCard() {
    if (this.isValidCCRegister()) {
      let cardDate = this.cardExpiration.split('/');
      let data = { 
        cardNumber: this.cardNumber.replace(/ /g, ''), 
        expYear: cardDate[1].trim(),
        expMonth: cardDate[0].trim(),
        cvc: this.cardCvc,
        userId: this.loggedUser['_id'],
        userName: this.cardName,
        userEmail: this.loggedUser['email']
      };
      let loader = this._loaderCtrl.create({
        spinner: 'hide',
        content: `<div class="ede-loader">
                    <img src="assets/gifs/ede-loader.gif">
                    <span>Validando la tarjeta</span>
                  </div>`
      });
      loader.present();
      this._transactionService.createCreditCard(data, this.loggedUser.token).subscribe(async (response) => {
        console.log(response.data.response);
        if (response.status == "success" && response.data.response.status == "CREATED") {
          let creditCardData = {
            name: response.data.response.data.name,
            token: response.data.response.data.id,
            cardName: response.data.response.data.card_holder
          }
          if (!this.availableCreditCards) {
            this.availableCreditCards = [];
          }
          this.availableCreditCards.push(creditCardData);
          await this._storage.set("my-credit-cards", this.availableCreditCards);
          loader.dismiss();
          this.addNewCard = false;
          this.cardNumber;
          this.cardCvc = "";
          this.cardExpiration = "";
          this.cardName = "";
          let alert = this._alertCtrl.create({ 
            title: 'Operacion exitosa!',
            message: 
            `Se valido su tarjeta correctamente`,
            buttons: [{role: 'OK', text: 'Entendido'}]
          });
          this.creditCardSelected = creditCardData;
          alert.present();
        } else {
          let alert = this._alertCtrl.create({ 
            title: 'Error en la transacción',
            message: 
            `Ocurrio un error guardando la tarjeta, revisa los datos e intenta de nuevo`,
            buttons: [{role: 'OK', text: 'Entendido'}]
          });
          loader.dismiss();
          alert.present();
        }
      })
    } else {
      let alert = this._alertCtrl.create({ 
        title: 'Cuidado!',
        message: 
        `Por favor complete todos los datos de su tarjeta`,
        buttons: [{role: 'OK', text: 'Entendido'}]
      });
      alert.present();
    }
  }

  public async buyTicketCC() {
    if (this.hasRequiredData() && this.isValidCCPurchase()) {
      let loader = this._loaderCtrl.create({
        spinner: 'hide',
        content: `<div class="ede-loader">
                    <img src="assets/gifs/ede-loader.gif">
                    <span>Realizando la compra</span>
                  </div>`
      });
      if (this.validCellPhone()) {
        loader.present();
        const data = {
          paymentMethod: this.paymentMethod,
          token: this.creditCardSelected.token,
          eventId: this.eventToBuy['_id'],
          userId: this.loggedUser['_id'],
          value: this.totalPrice,
          userName: this.creditCardSelected.cardName,
          userLastname: this.creditCardSelected.cardLastname,
          userEmail: this.loggedUser['email'],
          eventName: this.eventToBuy['name'],
          userDoc: this.userDocument,
          stageNumber: this.currentTicketStage.index,
          installments: 1,
          userCellPhone: this.userCellPhone,
          eventImageSrc: this.eventToBuy['imageSrc']
        }
        this._transactionService.buyTicketWithCreditCard(data, this.loggedUser.token).subscribe(async (response: any) => {
          if (response.status == "success") {
            let myList = await this._storage.get("my-tickets");
            if (!myList) {
              myList = [];
            }
            this.eventToBuy.qr = response.qr;
            myList.push(this.eventToBuy);
            await this._storage.set("my-tickets", myList);
            loader.dismiss();
            this.navCtrl.setRoot('QrViewerPage', { event: this.eventToBuy, comeFromPayment: true });
          } else if (response.status == "pending") {
            let alert = this._alertCtrl.create({
              title: 'Transacción pendiente',
              message: 
              `Tu transacción ha quedado pendiente, una vez se haga efectiva recibirás tu ticket en la sección mis tickets`,
              buttons: [{role: 'OK', text: 'Entendido', handler: () => {this.navCtrl.setRoot('HomePage')}}]
            });
            this.pushPendingTicket(this.eventToBuy);
            loader.dismiss();
            alert.present();
          } else if (response.status == "declined") {
            let alert = this._alertCtrl.create({ 
              title: 'Oops! :(',
              message: response.message + " Intenta con otro medio de pago",
              buttons: [{role: 'OK', text: 'Entendido'}]
            });
            loader.dismiss();
            alert.present();
          } else if (response.status == "error") {
            let alert = this._alertCtrl.create({ 
              title: 'Error en la transacción',
              message: 
              `No pudimos validar la tarjeta de credito, intenta con otro medio de pago.`,
              buttons: [{role: 'OK', text: 'Entendido'}]
            });
            loader.dismiss();
            alert.present();
          }
        }, (error) => {
          let alert = this._alertCtrl.create({ 
            title: 'Error interno',
            message: 
            `No pudimos comunicar con el servidor, intente mas tarde, si el error persiste ponte en contacto con el administrador.`,
            buttons: [{role: 'OK', text: 'Entendido', handler: () => {this.navCtrl.setRoot('HomePage')}}]
          });
          loader.dismiss();
          alert.present();
        });
      } else {
        let alert = this._alertCtrl.create({
          title: 'Cuidado!',
          message: 
          `El número de celular debe tener 10 digitos`,
          buttons: [{role: 'OK', text: 'Entendido'}]
        });
        alert.present();
      }
    } else {
      let alert = this._alertCtrl.create({ 
        title: 'Cuidado!',
        message: 
        `Por favor complete todos los campos`,
        buttons: [{role: 'OK', text: 'Entendido'}]
      });
      alert.present();
    }
  }

  public async buyTicketCash() {
    if (this.hasRequiredData() && this.isValidCashPurchase()) {
      if (this.validCellPhone()) {
        let loader = this._loaderCtrl.create({
          spinner: 'hide',
          content: `<div class="ede-loader">
                      <img src="assets/gifs/ede-loader.gif">
                      <span>Solicitando el pin</span>
                    </div>`
        });
        const data = {
          entity: this.cashType,
          eventId: this.eventToBuy['_id'],
          userId: this.loggedUser['_id'],
          value: this.totalPrice,
          userName: this.loggedUser['name'],
          userLastname: this.loggedUser['lastname'],
          userEmail: this.loggedUser['email'],
          eventName: this.eventToBuy['name'],
          userDoc: this.userDocument,
          userCellPhone: this.userCellPhone
        }
        loader.present();
        this._transactionService.buyTicketWithCash(data, this.loggedUser.token).subscribe(async (response: any) => {
          await this.pushPendingTicket(this.eventToBuy);
          loader.dismiss();
          let alert = this._alertCtrl.create({ 
            title: 'Transacción exitosa',
            message: 
            `Dirijase a un punto ${this.cashType.toUpperCase()} y realice su pago con los siguientes datos: 
            <h2 style='font-weight': 'bold';>CÓDIGO DEL PROYECTO: ${response.data.projectCode}</h2> 
            <h2 style='font-weight': 'bold';>NÚMERO DE PIN: ${response.data.pin}</h2>
            luego de realizar el pago, podrá tardar hasta 1 hora para ver tu ticket en la sección 'Mis tickets'`,
            buttons: [{role: 'OK', text: 'Entendido', handler: () => {this.navCtrl.setRoot('HomePage')}}]
          });
          alert.present();
        }, (error) => {
          let alert = this._alertCtrl.create({ 
            title: 'Error en la transacción',
            message: 
            `Ocurrio un error creando el pin en la entidad, porfavor intente mas tarde o pongase en contacto con el administrador.`,
            buttons: [{role: 'OK', text: 'Entendido'}]
          });
          loader.dismiss();
          alert.present();
        });
      } else {
        let alert = this._alertCtrl.create({ 
          title: 'Cuidado!',
          message: 
          `El número de celular debe tener 10 digitos`,
          buttons: [{role: 'OK', text: 'Entendido'}]
        });
        alert.present();
      }
    } else {
      let alert = this._alertCtrl.create({ 
        title: 'Cuidado!',
        message: 
        `Por favor complete todos los campos`,
        buttons: [{role: 'OK', text: 'Entendido'}]
      });
      alert.present();
    }
  }

  public async buyTicketPSE() {
    if (this.hasRequiredData() && this.isValidPSEPurchase()) {
      if (this.validCellPhone()) {
        let loader = this._loaderCtrl.create({
          spinner: 'hide',
          content: `<div class="ede-loader">
                      <img src="assets/gifs/ede-loader.gif">
                      <span>Creando la transacción</span>
                    </div>`
        });
        loader.present();
        const data = {
          bank: this.bankName,
          eventId: this.eventToBuy['_id'],
          userId: this.loggedUser['_id'],
          value: this.totalPrice,
          userName: this.loggedUser['name'],
          userLastname: this.loggedUser['lastname'],
          userEmail: this.loggedUser['email'],
          eventName: this.eventToBuy['name'],
          userDoc: this.userDocument,
          userCellphone: this.userCellPhone
        };
        this._transactionService.buyTicketWithPSE(data, this.loggedUser.token).subscribe((response: any) => {
          loader.dismiss();
          if (response.status == "success") {
            if (this._platform.is('ios')) {
              let browser = window.open(response.data.urlbanco, '_blank');
              browser.addEventListener('exit', () => {
                this.verifyPSETransaction(response);
              });
            } else {
              let browser = this._iab.create(response.data.urlbanco);
              browser.on('exit').subscribe(()=> {
                this.verifyPSETransaction(response);
              })
            }
          }
        }, (error) => {
          let alert = this._alertCtrl.create({ 
            title: 'Oops!',
            message: 
            `Ocurrio un error creando la transacción, por favor intenta más tarde`,
            buttons: [{role: 'OK', text: 'Entendido'}]
          });
          alert.present();
        });
      } else {
        let alert = this._alertCtrl.create({ 
          title: 'Cuidado!',
          message: 
          `El número de celular debe tener 10 digitos`,
          buttons: [{role: 'OK', text: 'Entendido'}]
        });
        alert.present();
      }
    } else {
      let alert = this._alertCtrl.create({ 
        title: 'Cuidado!',
        message: 
        `Por favor complete todos los campos`,
        buttons: [{role: 'OK', text: 'Entendido'}]
      });
      alert.present();
    }
  }

  private async pushPendingTicket(ticket: object) {
    let pendingTickets = await this._storage.get('pending-tickets');
    if (!pendingTickets) {
      pendingTickets = [];
    }
    pendingTickets.push(ticket);
    await this._storage.set('pending-tickets', pendingTickets);
  }

  private getCurrentTicketsStage() {
    if (this.eventToBuy['tickets'] != null) {
      let result = null;
      this.eventToBuy['tickets'].map((stage, index) => {
        if (moment(new Date()).isBetween(stage.start_date, stage.end_date) || moment(stage.start_date).diff(new Date()) < 0) {
          if (stage.amount_sold < stage.amount_available) {
            result = stage;
            result.index = index;
          } else if (stage.amount_available == stage.amount_sold) {
            if (this.eventToBuy['tickets'][index + 1] && this.eventToBuy['tickets'][index + 1].amount_sold < this.eventToBuy['tickets'][index + 1].amount_available) {
              result = this.eventToBuy['tickets'][index + 1];
              result.index = index + 1;
            } else if (this.eventToBuy['tickets'][index + 2] && this.eventToBuy['tickets'][index + 2].amount_sold < this.eventToBuy['tickets'][index + 2].amount_available) {
              result = this.eventToBuy['tickets'][index + 2];
              result.index = index + 2;
            } else if (this.eventToBuy['tickets'][index + 3] && this.eventToBuy['tickets'][index + 3].amount_sold < this.eventToBuy['tickets'][index + 3].amount_available) {
              result = this.eventToBuy['tickets'][index + 3];
              result.index = index + 3;
            }
          }
        }
      });
      return result;
    } else {
      return null;
    }
  }

  private validCellPhone() {
    if (this.userCellPhone.length < 10) {
      return false;
    } else {
      return true;
    }
  }

  private hasRequiredData() {
    if (this.userDocumentType != "", this.userCellPhone != "" && this.userDocument != "" && this.paymentMethod != "") {
      return true;
    } else {
      return false;
    }
  }

  private isValidCCRegister() {
    if (this.paymentMethod == 'CREDIT_CARD' && this.cardNumber != "" && this.cardCvc != ""  && this.cardName != "") {
      return true;
    } else {
      return false;
    }
  }

  private isValidCCPurchase() {
    if (this.paymentMethod == 'CREDIT_CARD' && this.creditCardSelected) {
      return true;
    } else {
      return false;
    }
  }

  private isValidCashPurchase() {
    if (this.paymentMethod == "Cash" && this.cashType != "") {
      return true;
    } else {
      return false;
    }
  }

  private isValidPSEPurchase() {
    if (this.paymentMethod == "PSE" && this.bankName != "") {
      return true;
    } else {
      return false;
    }
  }

  private verifyPSETransaction(response) {
    let loader2 = this._loaderCtrl.create({
      spinner: 'hide',
      content: `<div class="ede-loader">
                  <img src="assets/gifs/ede-loader.gif">
                  <span>Consultando la transacción</span>
                </div>`
    });
    loader2.present();
    this._transactionService.getTransactionStatus(this.loggedUser['_id'], response.data.factura).subscribe(async (response) => {
      loader2.dismiss();
      // Transaccion aprobada
      if (response.data.data.estado == "Aceptada") {
        let myTickets = await this._storage.get("my-tickets");
        if (!myTickets) {
          myTickets = [];
        }
        this.eventToBuy.qr = response.data.data.ref_payco;
        myTickets.push(this.eventToBuy);
        await this._storage.set("my-tickets", myTickets);
        let alert = this._alertCtrl.create({ 
          title: 'Transacción aprobada!',
          message: 
          `El pago fue recibido en nuestro sistema, tu ticket ha sido generado, preparate para bailar!`,
          buttons: [{role: 'OK', text: 'Entendido', handler: () => {
            this.navCtrl.setRoot('QrViewerPage', { event: this.eventToBuy })
          }}]
        });
        alert.present();
      // Transaccion rechazada
      } else if (response.data.data.estado == "Rechazada") {
        let alert = this._alertCtrl.create({ 
          title: 'Oops, transacción rechazada!',
          message: 
          `Al parecer la transacción ha sido rechazada por el banco, prueba otro metodo`,
          buttons: [{role: 'OK', text: 'Entendido', handler: () => {
            this.navCtrl.setRoot('HomePage')
          }}]
        });
        alert.present();
      // Transaccion pendiente
      } else if (response.data.data.estado == "Pendiente") {
        let alert = this._alertCtrl.create({ 
          title: 'Transacción pendiente',
          message: 
          `No pudimos validar el pago en el momento, una vez aprobada tu ticket aparece en la sección 'Mis tickets'`,
          buttons: [{role: 'OK', text: 'Entendido', handler: () => {
            this.navCtrl.setRoot('HomePage')
          }}]
        });
        alert.present();
      // Transaccion fallida
      } else if (response.data.data.estado == "Fallida") {
        let alert = this._alertCtrl.create({ 
          title: 'Oops!',
          message: 
          `Al parecer tu transacción no pudo ser procesada, intenta de nuevo`,
          buttons: [{role: 'OK', text: 'Entendido', handler: () => {
            this.navCtrl.setRoot('HomePage')
          }}]
        });
        alert.present();
      }
    }, (error) => {
      let alert = this._alertCtrl.create({ 
        title: 'Oops!',
        message: 
        `Ocurrio un error consultando el estado de tu transacción, por favor ponte en contacto con el administrador de la plataforma`,
        buttons: [{role: 'OK', text: 'Entendido', handler: () => {
          this.navCtrl.setRoot('HomePage')
        }}]
      });
      alert.present();
    })
  }

  public goToWebView() {
    const admin = this._iab.create("http://b520396a.ngrok.io/paymentResponse/b8997850-6616-11e9-b56c-1f79d0f86904/5c39e79a9f36bd758087c691");
  }

  /*<ion-item>
      <ion-label>Cantidad de boletas</ion-label>
      <ion-select [(ngModel)]="ticketsQuantity" (ngModelChange)="recalculatePrice()">
        <ion-option value="1">1</ion-option>
        <ion-option value="2">2</ion-option>
      </ion-select>
    </ion-item>*/

}
