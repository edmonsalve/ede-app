import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutPage } from './checkout';

import {CardModule} from 'ngx-card/ngx-card';
import { TextMaskModule } from "angular2-text-mask";

@NgModule({
  declarations: [
    CheckoutPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckoutPage),
    CardModule,
    TextMaskModule
  ],
})
export class CheckoutPageModule {}
